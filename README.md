# Layer8 the open source network engineers ticketing tool.

Layer8 is an open source ticketing system built off of node.js specifically for network engineers. 
The project is intended to be modular with a core set of features including but not limited to,

DNS Requests,
Firewall Requests,
Firewall Block Requests,
IP address requests,
General network troubleshooting,
Advanced network troubleshooting.

More features will be added overtime however this short list should be core to any 
network engineer's day to day work.